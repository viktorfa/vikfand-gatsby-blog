// custom typefaces
import "typeface-montserrat"
import "typeface-merriweather"

// prismjs code highlighting
require("prismjs/themes/prism-solarizedlight.css")
